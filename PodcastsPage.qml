import QtQuick 2.7
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.1 as Kirigami

Kirigami.Page {
    id: podcastsPage
    title: "Podcasts"

    header: Controls.TabBar {
        id: podcastsTabBar
        currentIndex: podcastsSwipeView.currentIndex

        Controls.TabButton {
            text: "Subscriptions"
        }
        Controls.TabButton {
            text: "Add Podcast"
        }
    }

    Controls.SwipeView {
        id: podcastsSwipeView
        anchors.fill: parent
        currentIndex: podcastsTabBar.currentIndex

        Item {
            Controls.Label {
                text: qsTr("Add subscription")
                anchors.centerIn: parent
            }
        }

        AddPodcast {}

    }
}
