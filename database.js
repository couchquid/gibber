function dbInit() {
  var db = LocalStorage.openDatabaseSync("Gibber_DB", "1.0", "Podcast Database", 1000000)
  try {
    db.transaction(function (tx) {
      tx.executeSql('CREATE TABLE IF NOT EXISTS subscriptions (title text, url text)')
    })
  } catch (err) {
    console.log("Error creating table in database: " + err)
  };
}

function dbOpen() {
  try {
    var db = LocalStorage.openDatabaseSync("Gibber_DB", "1.0", "Podcast Database", 1000000)
  } catch (err) {
    console.log("Error opening database: " + err)
  }

  return db
}

function dbInsert(Ptitle, Purl) {
  var db = dbOpen()
  var rowId = 0;
  db.transaction(function (tx) {
    tx.executeSql('INSERT INTO subscriptions VALUES(?, ?)', [Ptitle, Purl])
    var result = tx.executeSql('SELECT last_insert_rowid()')
    rowId = result.insertId
  })

  return rowId;
}
