import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2 as Controls
import QtQuick.LocalStorage 2.0

import "database.js" as DB

Item {
    RowLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top

        Controls.TextField {
            id: urlField
            placeholderText: "Podcast URL"
        }

        Controls.Button {
            text: "Add Subscription"
            onClicked: {
                var rowid = parseInt(DB.dbInsert("asdf", urlField.text), 10)
                console.log(rowid)
            }
        }
    }
}
