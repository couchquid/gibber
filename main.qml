import QtQuick 2.7
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.0
import org.kde.kirigami 2.1 as Kirigami

import "database.js" as DB

Kirigami.ApplicationWindow {
    id: root
    width: 360
    height: 640

    globalDrawer: Kirigami.GlobalDrawer {
        title: "Gibber"
        titleIcon: "applications-graphics"
        drawerOpen: false

        actions: [
            Kirigami.Action {
                text: "Podcasts"
                checkable: true
                property bool current: pageStack.currentItem ? pageStack.currentItem.objectName == "podcastsPage" : false
                onCurrentChanged: {
                    checked = current;
                }
                onTriggered: {
                    pageStack.push(podcastsComponent);
                }
            },
            Kirigami.Action {
                text: "Preferences"
                checkable: true
                property bool current: pageStack.currentItem ? pageStack.currentItem.objectName == "preferencesPage" : false
                onCurrentChanged: {
                    checked = current;
                }
                onTriggered: {
                    pageStack.push(preferencesComponent);
                }
            }
        ]
    }

    pageStack.initialPage: mainPageComponent

    Component {
        id: preferencesComponent
        Kirigami.Page {
            title: "Preferences"
            objectName: "preferencesPane"
            Rectangle {
                anchors.fill: parent
                Controls.Button {
                    anchors.centerIn: parent
                    text: "Remove Page"
                    onClicked: applicationWindow().pageStack.pop();
                }
            }
        }
    }

    Component {
        id: mainPageComponent
        MainPage {}
    }

    Component {
        id: podcastsComponent
        PodcastsPage {}
    }


    Component.onCompleted: {
      DB.dbInit()
    }
}
